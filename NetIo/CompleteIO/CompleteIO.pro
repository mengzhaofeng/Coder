#-------------------------------------------------
#
# Project created by QtCreator 2014-10-29T21:10:43
#
#-------------------------------------------------

QT       -= gui

TARGET = CompleteIO
TEMPLATE = lib

DEFINES += COMPLETEIO_LIBRARY

SOURCES += completeio.cpp

HEADERS += completeio.h\
        completeio_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
