#include <iostream>
#include "thread_mgr.h"
#include "thread_synchronization.h"
#include "t_thread_specific_ptr.h"

int main(int argc, char *argv[])
{

    thread_mgr tm;

    //tm.test_create_thread();


    //tm.test_thread_sleep();


    //tm.test_thread_join_scoped();


    //tm.test_thread_interrupt();

    //tm.test_thread_others();

    thread_synchronization ts;

    //ts.test_without_lock();


    //ts.test_internal_lock();


    //ts.test_external_lock();


    //ts.test_recursive_lock();


    //ts.test_lock_function();


    //ts.test_try_lock();


    ts.test_timed_lock();


    //ts.test_shared_lock();


    //ts.test_upgrade_lock();


    //ts.test_unique_lock();


    //ts.test_shared_lock_1();


    //ts.test_condition_variable();


    //ts.test_call_one();


    //ts.test_barrier();


    //ts.test_latch();


    //ts.test_exector();


    //ts.test_future();


    T_thread_specific_ptr ttsp;

    //ttsp.test_thread_specific_ptr();

    return 0;
}
