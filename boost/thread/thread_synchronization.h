#ifndef THREAD_SYNCHRONIZATION_H
#define THREAD_SYNCHRONIZATION_H

class thread_synchronization
{
public:
    thread_synchronization();

    void test_without_lock();
    void test_internal_lock();
    void test_external_lock();

    void test_recursive_lock();
    void test_lock_function();

    void test_try_lock();

    void test_timed_lock();

    void test_shared_lock();

    void test_upgrade_lock();

    void test_unique_lock();

    void test_shared_lock_1();

    void test_condition_variable();

    void test_call_one();

    void test_barrier();

    void test_latch();

    void test_exector();

    void test_future();
};

#endif // THREAD_SYNCHRONIZATION_H
