#-------------------------------------------------
#
# Project created by QtCreator 2014-09-23T14:34:18
#
#-------------------------------------------------

QT       -= core

QT       -= gui

TARGET = thread
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

include (../boost.pri)


SOURCES += main.cpp \
    thread_mgr.cpp \
    thread_synchronization.cpp \
    t_thread_specific_ptr.cpp

HEADERS += \
    thread_mgr.h \
    thread_synchronization.h \
    t_thread_specific_ptr.h
