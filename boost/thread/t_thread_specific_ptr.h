#ifndef T_THREAD_SPECIFIC_PTR_H
#define T_THREAD_SPECIFIC_PTR_H

class T_thread_specific_ptr
{
public:
    T_thread_specific_ptr();

    void test_thread_specific_ptr();
};

#endif // T_THREAD_SPECIFIC_PTR_H
