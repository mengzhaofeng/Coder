#include "t_thread_specific_ptr.h"
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

T_thread_specific_ptr::T_thread_specific_ptr()
{
}

///
/// \brief clean_func
/// \param t
///
void clean_func(int *t)
{
    std::cout << "clean: " << *t << std::endl;
}

///
/// \brief tss
///
boost::thread_specific_ptr<int> tss(&clean_func);

///
/// \brief test_tss
///
void test_tss(int v)
{
    tss.reset(&v);

    for(int i=0; i<5; i++)
    {
        boost::this_thread::sleep_for(boost::chrono::seconds(1));
        std::cout << "thread " <<v
                  << ": value = " << *tss << std::endl;
        std::cout.flush();
    }
}



void T_thread_specific_ptr::test_thread_specific_ptr()
{
    boost::thread t1(&test_tss, 5);
    boost::thread t2(&test_tss, 6);
    boost::thread t3(&test_tss, 7);

    boost::this_thread::sleep_for(boost::chrono::seconds(10));

    std::cout << "main thread done..." << std::endl;


}
