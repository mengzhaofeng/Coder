#ifndef THREAD_MGR_H
#define THREAD_MGR_H


class thread_mgr
{
public:
    thread_mgr();

    void test_create_thread();


    void test_thread_sleep();


    void test_thread_join_scoped();


    void test_thread_interrupt();


    void test_thread_others();
};

#endif // THREAD_MGR_H
