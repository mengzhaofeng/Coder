THIRD_PARTY = ../../../../vslib

DESTDIR = ../bin

BOOST_DIR = $${THIRD_PARTY}/boost_1.56

INCLUDEPATH += $${BOOST_DIR}/include
QMAKE_INCDIR += $${BOOST_DIR}/include
QMAKE_LIBDIR += $${BOOST_DIR}/lib

OTHER_FILES +=
