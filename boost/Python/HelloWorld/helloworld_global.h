#ifndef HELLOWORLD_GLOBAL_H
#define HELLOWORLD_GLOBAL_H


#if defined(PYTHON_LIBRARY)
#  define PYTHON_EXPORT __declspec(dllexport)
#else
#  define PYTHON_EXPORT __declspec(dllimport)
#endif
#endif // HELLOWORLD_GLOBAL_H
