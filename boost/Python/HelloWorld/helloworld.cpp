#include "helloworld.h"
#include <boost/python.hpp>

///******************************************************************///
/// \brief greet
/// \return
///
char const* greet()
{
   return "hello, world";
}

///*****************************************************************///
/// \brief The World struct
///
struct World
{
    void set(std::string msg) { this->msg = msg; }
    std::string greet() { return msg; }
    std::string msg;
};


BOOST_PYTHON_MODULE(HelloWorld)
{
    using namespace boost::python;

    def("greet", greet);

    class_<World>("World")
        .def("greet", &World::greet)
        .def("set", &World::set)
    ;
}
