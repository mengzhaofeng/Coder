#-------------------------------------------------
#
# Project created by QtCreator 2014-09-30T16:43:06
#
#-------------------------------------------------

QT       -= core gui

TARGET = HelloWorld
TEMPLATE = lib

DEFINES += PYTHON_LIBRARY

include (../python.pri)

QMAKE_POST_LINK += "cmd /c ..\post_link.bat $${TARGET}.dll $${TARGET}.pyd"

SOURCES += helloworld.cpp

HEADERS += helloworld.h\
        helloworld_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
