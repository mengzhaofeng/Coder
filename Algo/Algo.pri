THIRD_PARTY = ../../../../vslib

DESTDIR = ../bin

BOOST_DIR = $${THIRD_PARTY}/boost_1.56
PYTHON_DIR = $${THIRD_PARTY}/python27
GMPFR_DIR = $${THIRD_PARTY}/gmp

INCLUDEPATH += $${BOOST_DIR}/include
QMAKE_INCDIR += $${BOOST_DIR}/include
QMAKE_LIBDIR += $${BOOST_DIR}/lib


INCLUDEPATH += $${PYTHON_DIR}/include
QMAKE_INCDIR += $${PYTHON_DIR}/include
QMAKE_LIBDIR += $${PYTHON_DIR}/lib

INCLUDEPATH += $${GMPFR_DIR}/include
QMAKE_INCDIR += $${GMPFR_DIR}/include
QMAKE_LIBDIR += $${GMPFR_DIR}/lib

LIBS += python27.lib libgmp-10.lib

QMAKE_CXXFLAGS += -wd4100
