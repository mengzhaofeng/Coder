#-------------------------------------------------
#
# Project created by QtCreator 2014-07-09T11:13:28
#
#-------------------------------------------------

QT       -= gui

TARGET = PyAlgoSort
TEMPLATE = lib

include (../Algo.pri)

QMAKE_CXXFLAGS += -wd4100 -wd4996 -wd4146



DEFINES += PYALGO_LIBRARY

SOURCES += pyalgo.cpp \
    sort.cpp

HEADERS += pyalgo.h\
    sort.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
