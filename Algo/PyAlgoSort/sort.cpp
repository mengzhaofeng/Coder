#include "sort.h"
#include "EClock.hpp"
#include <boost/smart_ptr.hpp>



/****************************************************************************************************************/
/*!
 * 插入排序
 */
double InsertionSort(int *data, int size)
{
    double res = 0;
    int tmp;
    int j;

    EClock<> Ek;
    for(int i=1; i<size; i++) {
        tmp = data[i];
        j = i-1;
        while(j>=0 && data[j] > tmp) {
            data[j+1] = data[j];
            j--;
        }
        data[j+1] = tmp;
    }
    res = Ek.microsecond();
    return res;
}

/****************************************************************************************************************/
/*!
 * 选择排序
 */
double SelectionSort(int *data, int size)
{
    double res = 0;
    int tmp;
    int K;

    EClock<> Ek;
    for(int i=0; i<size-1; i++) {
        K = i;
        for(int j=i+1; j<size; j++) {
            if(data[K] > data[j]) {
                K = j;
            }
        }
        if(K > i) {
            tmp = data[K];
            data[K] = data[i];
            data[i] = tmp;
        }
    }
    res = Ek.microsecond();
    return res;
}

/****************************************************************************************************************/
/*!
 * 冒泡排序
 */
double BubbleSort(int *data, int size)
{
    double res = 0;
    int tmp;

    EClock<> Ek;
    for(int i=0; i<size-1; i++) {
        for(int j=0; j<size-i-1; j++) {
            if(data[j] > data[j+1]) {
                tmp = data[j];
                data[j] = data[j+1];
                data[j+1] = tmp;
            }
        }
    }
    res = Ek.microsecond();

    return res;
}

/****************************************************************************************************************/
/*!
*  from wiki
*/
void quick_sort1 (int data[], size_t left,size_t right)
{
    size_t p = (left + right) / 2;
    int pivot = data[p];
    size_t i = left,j = right;
    for ( ; i < j;) {
        while (! (i>= p || pivot < data[i]))
            ++i;
        if (i < p) {
            data[p] = data[i];
            p = i;
        }
        while (! (j <= p || data[j] < pivot))
            --j;
        if (j > p) {
            data[p] = data[j];
            p = j;
        }
    }
    data[p] = pivot;
    if (p - left > 1)
        quick_sort1 (data, left, p - 1);
    if (right - p > 1)
        quick_sort1 (data, p + 1, right);
}

/*!
 * implemented by mengzhaofeng, 正序或者逆序时性能很差O(n^2),数据分布比较随机时情况较好,受样本分布影响比较大，不稳定
 */
void quick_sort2(int *data, int first, int last)
{
    int i = first, j = last-1, k = last;
    int base = data[last];
    while(i <= j) {
        while(data[i] <= base && i < k) i++;
        if(i < k) {
            data[k] = data[i];
            k = i;
            i++;
        }

        while(data[j] >= base && j > k) j--;
        if(j > k) {
            data[k] = data[j];
            k = j;
            j--;
        }

    }
    data[k] = base;
    if(k > first+1) quick_sort2(data, first, k-1);
    if(k < last-1) quick_sort2(data, k+1, last);
}

/*!
 * 快速排序
 */
double QuickSort(int *data, int size)
{
    double res = 0;
    EClock<> Ek;
    quick_sort1(data, 0, size-1);
    res = Ek.microsecond();
    return res;
}
/****************************************************************************************************************/

/*!
 * implemented by mengzhaofeng
 */
void merge_sort(int *data, int left, int right, int *temp)
{
    if(left == right) return ;
    if(right-left == 1) {
        if(data[left] > data[right]) {
            int tmp = data[left];
            data[left] = data[right];
            data[right] = tmp;
        }
        return ;
    }

    int m = ((left + right) / 2);

    merge_sort(data, left, m, temp);
    merge_sort(data, m+1, right, temp);

    //merge

    int i = left;
    int k = left;
    int j = m + 1;

    while(i <= m) {
        if(j <= right) {
            if(data[i] < data[j]) {
                temp[k++] = data[i++];
            } else {
                temp[k++] = data[j++];
            }
        } else break;
    }

    while(i <= m) {
        temp[k++] = data[i++];
    }

    while(j <= right) {
        temp[k++] = data[j++];
    }

    i = left;
    j = left;
    while(i <= right) {
        data[i++] = temp[j++];
    }
}
/*!
 * 归并排序
 */
double MergeSort(int *data, int size)
{
    double res = 0;
    boost::scoped_array<int> temp(new int[size]);
    EClock<> Ek;
    merge_sort(data, 0, size-1, temp.get());
    res = Ek.microsecond();
    return res;
}

/****************************************************************************************************************/



/*!
 * \brief from wiki
 * \param d
 * \param ind
 * \param len
 */
void sift(int d[], int ind, int len)
{
    //#置i为要筛选的节点#%
    int i = ind;

    //#c中保存i节点的左孩子#%
    int c = i * 2 + 1; //#+1的目的就是为了解决节点从0开始而他的左孩子一直为0的问题#%

    while(c < len)//#未筛选到叶子节点#%
    {
        //#如果要筛选的节点既有左孩子又有右孩子并且左孩子值小于右孩子#%
        //#从二者中选出较大的并记录#%
        if(c + 1 < len && d[c] < d[c + 1])
            c++;
        //#如果要筛选的节点中的值大于左右孩子的较大者则退出#%
        if(d[i] > d[c]) break;
        else
        {
            //#交换#%
            int t = d[c];
            d[c] = d[i];
            d[i] = t;

            //#重置要筛选的节点和要筛选的左孩子#%
            i = c;
            c = 2 * i + 1;
        }
    }

    return;
}

void heap_sort(int d[], int n)
{
    //#初始化建堆, i从最后一个非叶子节点开始#%
    for(int i = (n - 2) / 2; i >= 0; i--)
        sift(d, i, n);

    for(int j = 0; j < n; j++)
    {
        //#交换#%
        int t = d[0];
        d[0] = d[n - j - 1];
        d[n - j - 1] = t;

        //#筛选编号为0 #%
        sift(d, 0, n - j - 1);

    }
}

/*!
 * 堆排序
 */
double HeapSort(int *data, int size)
{
    double res = 0;

    EClock<> Ek;

    heap_sort(data, size);

    res = Ek.microsecond();

    return res;
}


/****************************************************************************************************************/

/*!
 * 计数排序,样本中不能出现负数
 */
double CountingSort(int *data, int size)
{
    double res = 0;
    int max = data[0];
    int min = data[0];
    for(int i=1; i<size; i++)
    {
        if(max < data[i])
        {
            max = data[i];
        }
        if(min > data[i])
        {
            min = data[i];
        }
    }
    min < 0 ? (max = max - min + 1) : (max += 1);
    
    boost::scoped_array<int> temp(new int[max]);

    EClock<> Ek;
    for(int i=0; i<max; i++)
    {
        temp[i] = 0;
    }
    for(int i=0; i<size; i++)
    {
        temp[(min < 0 ? (data[i]-min) : data[i])] += 1;
    }
    for(int i=1; i<max; i++)
    {
        temp[i] += temp[i-1];
    }

    for(int i=max-1; i>0; i--)
    {
        while(temp[i]-temp[i-1]>0)data[--temp[i]] = min < 0 ? (i+min) : i;
    }
    while(temp[0]>0)data[--temp[0]] = min < 0 ? (0+min) : 0;
    printf("\n");

    res = Ek.microsecond();

    return res;
}

/****************************************************************************************************************/

/*!
 * 十进制基数排序
 */
double RadixSort(int *data, int size)
{
    double res = 0;

    int tmp;
    EClock<> Ek;
    int max = data[0];
    int min = data[0];
    for(int i=1; i<size; i++)
    {
        if(max < data[i])
        {
            max = data[i];
        }
        if(min > data[i])
        {
            min = data[i];
        }
    }

    if(min < 0)
    {
        for(int i=1; i<size; i++)
        {
            data[i] -= min;
        }
    }
    
    boost::scoped_array<int> temp(new int[size]);
    for(int i=0; i<size; i++) temp[i] = 0;
    
    min < 0 ? (max = max - min) : (max = max);

    int bary[10];
    int base = 10;
    int times = 0;
    tmp = max;
    do{ times++; }while(tmp/=base);
    tmp = 1;

    int *pd = data;
    int *pt = temp.get();
    int *pm = 0;    //printf("times = %d\n", times);
    while(times--)
    {
        for(int i=0; i<10; i++)
            bary[i] = 0;

        for(int i=0; i<size; i++)
            bary[(pd[i]/tmp)%10] += 1;

        for(int i=1; i<10; i++)
            bary[i] += bary[i-1];


        for(int i=size-1; i>=0; i--)
        {
            pt[--bary[(pd[i]/tmp)%10]] = pd[i];
        }
        pm = pd;
        pd = pt;
        pt = pm;

        break;
        tmp *= 10;
    }
    if(pd != data)
    {
        for(int i=0; i<size; i++)
        {
            data[i] = pd[i];
        }
    }
    if(min < 0)
    {
        for(int i=0; i<size; i++)
        {
            data[i] = data[i] - min;
        }
    }

    res = Ek.microsecond();
    return res;
}





/****************************************************************************************************************/

/*!
 * 希尔排序
 */

/*!
 * \brief Fibonacci
 * 根据高德纳（Donald Ervin Knuth）的《计算机程序设计艺术》（The Art of Computer Programming），
 * 1150年印度数学家Gopala和金月在研究箱子包装物件长阔刚好为1和2的可行方法数目时，
 * 首先描述这个数列。 在西方，最先研究这个数列的人是比萨的列奥那多（又名费波那西），
 * 他描述兔子生长的数目时用上了这数列。
 * 第一个月初有一对刚诞生的兔子
 * 第二个月之后（第三个月初）它们可以生育
 * 每月每对可生育的兔子会诞生下一对新兔子
 * 兔子永不死去
 * 假设在n月有可生育的兔子总共a对，n+1月就总共有b对。在n+2月必定总共有a+b对：
 * 因为在n+2月的时候，前一月（n+1月）的b对兔子可以存留至第n+2月（在当月属于新诞生的兔子尚不能生育）。
 * 而新生育出的兔子对数等于所有在n月就已存在的a对
 * \param n
 * \return
 */
inline int Fibonacci(int n)
{
    if(n == 0) return 0;
    if(n == 1) return 1;
    return Fibonacci(n-1) + Fibonacci(n-2);
}


/*!
 * \brief Shell_Sequence
 * 斐波那契数列除去0和1将剩余的数以黄金分区比的两倍的幂进行运算
 * \param n
 * \return
 */
int Shell_Sequence(int n)
{
    if(n <= 0) return 0;
    return (int)pow(Fibonacci(n+1),(1+sqrt(5.0)));
}

double ShellSort(int *data, int size)
{
    double res = 0;

    int tmp = size/2;
    int sq[12] = {0};
    int n = 0;

    for(int i=1; i<13; i++)
    {
        sq[i-1] = Shell_Sequence(i);
        if(sq[i-1] > tmp)
        {
            n = i-1;
            break;
        }
    }

    EClock<> Ek;
    for(int i=n-1; i>=0; i--)
    {
        int m = size/sq[i]+1;
        for(int j=0; j<sq[i]; j++)
        {
            for(int k=1; k<m; k++)
            {
                if(sq[i]*k + j < size)
                {
                    int x = k;
                    tmp = data[sq[i]*x+j];
                    while(x > 0 && tmp < data[sq[i]*(x-1)+j])
                    {
                        data[sq[i]*x+j] = data[sq[i]*(x-1)+j];
                        x--;
                    }
                    data[sq[i]*x+j] = tmp;
                }
            }
        }
    }

    res = Ek.microsecond();
    return res;
}
