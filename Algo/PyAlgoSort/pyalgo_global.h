#ifndef PYALGO_GLOBAL_H
#define PYALGO_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PYALGO_LIBRARY)
#  define PYALGOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define PYALGOSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // PYALGO_GLOBAL_H
