#-------------------------------------------------
#
# Project created by QtCreator 2014-10-28T11:09:26
#
#-------------------------------------------------

QT       -= core gui

TARGET = StringSearch
TEMPLATE = lib

include (../Algo.pri)

QMAKE_POST_LINK += "cmd /c ..\post_link.bat $${TARGET}.dll $${TARGET}.pyd"

SOURCES += \
    KMP_BM.cpp

HEADERS += \
    KMP_BM.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES += \
    ../post_link.bat
